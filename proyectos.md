## Proyecto 1 - Análisis de sentimiento de redes sociales para las empresas

Dirigido a empresas.

Las empresas necesitan saber el impacto que tienen en la sociedad, de esta manera pueden evaluar si los productos que ofertan generan un tipo de sentimiento positivo en sus clientes.

La IA se encargará de analizar los comentarios de las personas en las distintas redes sociales de la empresa, por ejemplo si saca un nuevo producto, verá si tiene un impacto positivo o negativo.

**Forma de monetizar**: 50 publicaciones - 5 USD, 100 publicaciones - 10 USD.

Ref: https://docs.aws.amazon.com/es_es/comprehend/latest/dg/how-sentiment.html

https://aws.amazon.com/es/getting-started/hands-on/analyze-sentiment-comprehend/

## Proyecto 2 - Ofertas y búsqueda de trabajo en tiempo real con reconocimiento artificial

Dirigido a personas naturales.

Las personas pueden solicitar un trabajador tan solo sacando una foto al lugar de trabajo o a la herramienta. La IA se encargará de buscar una persona del rubro, e indicar un mapa en donde se encuentra disponible la persona (similar cuando se pide un Uber).

**Forma de monetizar**: Mensualidad a los trabajadores que ofrecen empleo en la plataforma.

Ref: https://aws.amazon.com/es/rekognition/?c=ml&sec=srv

## Proyecto 3 - Análisis de rostros mediante uso de IA para seguridad en kinders

Dirigido a kinders, nidito.

La IA analizará cada rostro de persona que ingresen al kinder (o institución para infantes) y lo emparejará con una base de datos ya definida de las personas autorizadas que puedan recoger a los niños.  

**Forma de monetizar**: Mensual, cantidad de personas a analizar: 100 rostros - 5 USD, 200 rostros 10 USD.

Ref: https://docs.aws.amazon.com/rekognition/latest/dg/collections.html?pg=ln&sec=ft
